FROM wishartlab/cfmid:latest

RUN apk update && apk add socat

COPY script.sh /cfmid/script.sh

CMD socat tcp-l:7777,fork system:/cfmid/script.sh