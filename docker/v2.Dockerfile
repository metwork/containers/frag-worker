FROM ubuntu

RUN apt update && apt-get install -y socat

RUN mkdir cfmid
COPY cfm_id /cfmid
COPY v2.script.sh /cfmid/script.sh
WORKDIR /cfmid
ENV PATH="/cfmid:${PATH}"

CMD socat tcp-l:7777,fork system:/cfmid/script.sh