import subprocess
import shlex


def run() -> None:
    print("### Run Flake8 ###")
    cmd = "flake8 frag_worker"
    subprocess.run(shlex.split(cmd))

    print("### Run MyPy ###")
    cmd = "mypy --strict frag_worker"
    subprocess.run(shlex.split(cmd))

    print("### Run pytest ###")
    cmd = "pytest --cov-report term-missing --cov=frag_worker --disable-warnings"
    subprocess.run(shlex.split(cmd))
