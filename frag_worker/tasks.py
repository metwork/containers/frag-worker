import json
from typing import Dict, Any

from celery import Celery
import requests
from matchms.exporting.save_as_json import SpectrumJSONEncoder
from cfm_id import CfmIdTCP

from frag_worker.settings import Settings

app = Celery("hello", broker="amqp://cfmid:BROKER_PASSWORD@cfmid_broker/cfmid")


def create_cfm_id() -> CfmIdTCP:
    settings = Settings()
    cfm_id = CfmIdTCP()
    cfm_id.params_dir = settings.CFMID_PARAMS_DIR
    cfm_id.set_predict_hash()
    return cfm_id


@app.task(name="predict")
def predict(smiles: str, params: Dict[Any, Any], callback: Dict[Any, Any]) -> str:
    cfm_id = create_cfm_id()
    spectra = cfm_id.predict(smiles)
    spectra_str = json.dumps(spectra, cls=SpectrumJSONEncoder)
    spectra_json = json.loads(spectra_str)
    for spectrum in spectra_json:
        peaks_json = spectrum.pop("peaks_json")
        data = {
            "smiles": smiles,
            "cfm_predict_hash": cfm_id.predict_hash,
            "spectrum_metadata": spectrum,
            "peaks_json": peaks_json,
        }
        response = requests.post(callback["url"], json=data)
        print(response.status_code)
    return spectra_json


if __name__ == "__main__":
    print(predict("CCOCCCC"))
