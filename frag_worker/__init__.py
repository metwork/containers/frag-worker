import pkg_resources

__version__ = pkg_resources.get_distribution("frag_worker").version
