from pydantic import BaseSettings


class Settings(BaseSettings):

    CFMID_PARAMS_DIR: str
